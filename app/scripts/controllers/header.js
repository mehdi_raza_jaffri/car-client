'use strict';

/**
 * @ngdoc function
 * @name carClientApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the carClientApp
 */
angular.module('carClientApp')
    .controller('headerCtrl', ['$scope', 'carService', '$location', '$rootScope',
    function ($scope, carService, $location, $rootScope) {
            $scope.data = {
                brands: {}
            };
            var substringMatcher = function (strs) {
                return function findMatches(q, cb) {
                    var matches, substrRegex;

                    // an array that will be populated with substring matches
                    matches = [];

                    // regex used to determine if a string contains the substring `q`
                    substrRegex = new RegExp(q, 'i');

                    // iterate through the pool of strings and for any string that
                    // contains the substring `q`, add it to the `matches` array
                    $.each(strs, function (i, str) {
                        if (substrRegex.test(str)) {
                            matches.push(str);
                        }
                    });

                    cb(matches);
                };
            };

            carService.init().then(function (response) {
                $scope.data.brands = carService.getBrands();
                $('#brandSearch').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                }, {
                    name: 'brands',
                    source: substringMatcher($scope.data.brands)
                });
                $('#brandSearch').bind('typeahead:select', function (ev, suggestion) {
                    $location.path('/make').search({
                        make: suggestion
                    });
                    $rootScope.$apply();
                });
            });




}]);