angular.module('carClientApp')
    .service('carService', ['$http', function ($http) {
        var totalData;
        var brands;

        var getData = function () {
            var url = "https://api.edmunds.com/api/vehicle/v2/makes?fmt=json&api_key=phs44h7rg4enu8pqtkn8x964&state=new&view=full";
            return $http.get(url).then(function (response) {
                totalData = response.data.makes;
                return totalData;
            });
        };

        var getTotalData = function () {
            return totalDatal;
        }

        var getBrands = function () {
            return _.map(totalData, function (value, index) {
                return value.name;
            });
        };
        var getCars = function (make) {
            var car = _.find(totalData, function (value) {
                return value.name == make;
            });
            console.log(car);
            return car;
        };
        var getImages = function (tag) {
            var url = "http://api.flickr.com/services/feeds/photos_public.gne?tag=" + tag + "&format=json&tagmode=any";
            return $http.get(url).then(function (response) {
                return response.data.items[0].media.m
            });
        };
        return {
            init: getData,
            getAll: getTotalData,
            getCars: getCars,
            getBrands: getBrands,
            getImages: getImages
        }
    }]);
